# README #

LcdOff is a simple tool that allows you to turn your LCD monitor(s) off.

### Usage ###

```
LcdOff [--help|--go]:
  --help                show this help message
  --go                  turn off display now
```

### Installation

LcdOff doesn't require installation.
Just [download](https://yadi.sk/d/TNaNR1BV3L6qbZ) and run.
