﻿/*******************************************************************
* Copyright (c) 2011 LcdOff Project
* License: BSD (see license.txt)
* $Author$
* $Revision$
* $HeadURL$
* $Date$
/******************************************************************/
#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#	pragma warning(push, 4)
#	pragma warning(disable: 4996)
#	pragma warning(disable: 4512)
#	pragma warning(disable: 4458) // declaration of '%s' hides class member
#	pragma warning(disable: 4456) // declaration of '%s' hides previous local declaration
#endif
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/classification.hpp>
#include <boost/program_options.hpp>
#include <boost/lexical_cast.hpp>
#include <windows.h>
#include "resource.h"
#include "tray.hpp"

#ifdef BOOST_PROGRAM_OPTIONS_NO_LIB
#include <libs/program_options/src/options_description.cpp>
#include <libs/program_options/src/value_semantic.cpp>
#include <libs/program_options/src/cmdline.cpp>
#include <libs/program_options/src/positional_options.cpp>
#include <libs/program_options/src/parsers.cpp>
#include <libs/program_options/src/convert.cpp>
#include <libs/program_options/src/utf8_codecvt_facet.cpp>
#include <libs/program_options/src/variables_map.cpp>
#include <libs/program_options/src/config_file.cpp>
#endif


class TurnOffButton : public Tray
{
public:
	TurnOffButton()
	{
		setIcon(IDI_ICON1);
		setToolTip(IDS_TURNOFF);
	}

	void onMenuClick(int MenuId)
	{
		if(MenuId == ID_TRAYMENU_EXIT) {
			DestroyWindow(hWndMsg_);
		} else if(MenuId == ID_TRAYMENU_TURNOFF) {
			displayTurnOff();
		}
	}

	void onLeftButtonDblClick()
	{
		displayTurnOff();
	}

	void onRightButtonDown()
	{
		POINT cursor;
		GetCursorPos(&cursor);
		HMENU menu = LoadMenu(hInstance_, MAKEINTRESOURCE(IDR_MENU1));
		TrackPopupMenu(GetSubMenu(menu, 0), 0, cursor.x, cursor.y, 0, hWndMsg_, NULL);
		DestroyMenu(menu);
	}

public:
	static void displayTurnOff()
	{
		Sleep(500);
		SendMessage(HWND_BROADCAST, WM_SYSCOMMAND, SC_MONITORPOWER, 2);
	}
};


template<typename Description>
int usage(const Description& desc, std::string info, int icon, int ans)
{
	std::wstring msg(info.begin(), info.end());
	if(info.length() > 0)
		msg += L"\n\n";
	info = boost::lexical_cast<std::string>(desc);
	msg.append(info.begin(), info.end());

	std::wcout << msg << std::endl;
	MessageBox(NULL, msg.c_str(), L"LcdOff", icon | MB_OK);
	return ans;
}

int APIENTRY WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
	using namespace boost::program_options;

	options_description desc("LcdOff [--help|--go]");
	desc.add_options()
		("help", "show this help message")
		("go", "turn off display now");
	variables_map vm;
	std::vector<std::wstring> cmds;
	std::wstring cmd(GetCommandLine());
	boost::split(cmds, cmd, boost::is_any_of(L" \t"));

	try {
		store(wcommand_line_parser(cmds).options(desc).run(), vm);
		if(vm.count("help")) {
			return usage(desc, "", MB_ICONINFORMATION, 0);
		} else if(vm.count("go")) {
			TurnOffButton::displayTurnOff();
			return 0;
		}
	} catch(const std::exception& ex) {
		return usage(desc, ex.what(), MB_ICONWARNING, -1);
	} catch(...) {
		return usage(desc, "Unknown error", MB_ICONERROR, -1);
	}

	TurnOffButton button;
	button.loop();
	return 0;
}
