﻿/*******************************************************************
* Copyright (c) 2011 LcdOff Project
* License: BSD (see license.txt)
* $Author$
* $Revision$
* $HeadURL$
* $Date$
/******************************************************************/
#ifndef _TRAY_HPP_
#define _TRAY_HPP_

#include <string>
#include <boost/noncopyable.hpp>
#include <windows.h>


class Tray : boost::noncopyable
{
public:
	Tray() : hInstance_(GetModuleHandle(NULL))
	{
		classRegister();
		hWndMsg_ = CreateWindow(TEXT("TrayResidentWnd"), TEXT(""), 0, 0, 0, 0, 0, NULL, NULL, hInstance_, NULL);
		SetWindowLong(hWndMsg_, GWL_USERDATA, (LONG)this);
		nid_.cbSize = sizeof(nid_);
		nid_.hWnd = hWndMsg_;
		nid_.uID = 0;
		nid_.uFlags = NIF_MESSAGE;
		nid_.uCallbackMessage = WM_TRAYICONMSG;
		Shell_NotifyIcon(NIM_ADD, &nid_);
		nid_.uVersion = NOTIFYICON_VERSION;
		Shell_NotifyIcon(NIM_SETVERSION, &nid_);
	}

	virtual ~Tray()
	{
		nid_.uFlags = 0;
		Shell_NotifyIcon(NIM_DELETE, &nid_);
	}

	void loop() const
	{
		MSG msg;
		while(GetMessage(&msg, NULL, 0, 0)) {
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	void showInfo(const std::wstring& title, const std::wstring& text)
	{
		nid_.uFlags = NIF_INFO;
		memcpy(nid_.szInfoTitle, title.c_str(), title.length() * sizeof(wchar_t));
		nid_.szInfoTitle[title.length()] = 0;
		memcpy(nid_.szInfo, text.c_str(), text.length() * sizeof(wchar_t));
		nid_.szInfo[text.length()] = 0;
		nid_.uTimeout = 0;
		nid_.dwInfoFlags = NIIF_INFO;
		Shell_NotifyIcon(NIM_MODIFY, &nid_);
	}

	void setIcon(int IconId)
	{
		nid_.uFlags |= NIF_ICON;
		nid_.hIcon = LoadIcon(hInstance_, MAKEINTRESOURCE(IconId));
		Shell_NotifyIcon(NIM_MODIFY, &nid_);
	}

	void setToolTip(const std::wstring& tip)
	{
		nid_.uFlags |= NIF_TIP;
		memcpy(nid_.szTip, tip.c_str(), tip.length() * sizeof(wchar_t));
		nid_.szTip[tip.length()] = 0;
		Shell_NotifyIcon(NIM_MODIFY, &nid_);
	}

	void setToolTip(int TipId)
	{
		const int SIZE = sizeof(nid_.szTip) / sizeof(wchar_t);
		wchar_t buf[SIZE];
		LoadString(hInstance_, TipId, buf, SIZE);
		setToolTip(buf);
	}

private:
	static LRESULT CALLBACK windowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
	{
		Tray *tray = (Tray *)GetWindowLong(hwnd, GWL_USERDATA);
		if(tray != NULL) {
			tray->WndProc(uMsg, wParam, lParam);
		}
		return DefWindowProc(hwnd, uMsg, wParam, lParam);
	}

	static bool classRegister()
	{
		static bool registered = false;
		if(!registered) {
			WNDCLASSEX wcx;
			memset(&wcx, 0, sizeof(wcx));
			wcx.cbSize = sizeof(wcx);
			wcx.style = 0;
			wcx.lpfnWndProc = Tray::windowProc;
			wcx.cbClsExtra = 0;
			wcx.cbWndExtra = 0;
			wcx.hInstance = GetModuleHandle(NULL);
			wcx.hIcon = wcx.hIconSm = NULL;
			wcx.hCursor = NULL;
			wcx.hbrBackground = NULL;
			wcx.lpszMenuName = NULL;
			wcx.lpszClassName = TEXT("TrayResidentWnd");
			if(!RegisterClassEx(&wcx))
				return false;
			registered = true;
		}
		return registered;
	}

protected:
	virtual LRESULT WndProc(UINT uMsg, WPARAM wParam, LPARAM lParam)
	{
		switch(uMsg) {
			case WM_TRAYICONMSG:
				switch(lParam) {
			case WM_RBUTTONDOWN:
				onRightButtonDown();
				break;
			case WM_LBUTTONDBLCLK:
				onLeftButtonDblClick();
				break;
				}
				break;
			case WM_COMMAND:
				onMenuClick(LOWORD(wParam));
				break;
			case WM_DESTROY:
				PostQuitMessage(0);
				break;
		};
		return 0;
	}

public:
	virtual void onRightButtonDown() { }
	virtual void onLeftButtonDblClick() { }
	virtual void onMenuClick(int /*MenuId*/) { }

protected:
	HWND hWndMsg_;
	HINSTANCE hInstance_;
	NOTIFYICONDATA nid_;
	enum { WM_TRAYICONMSG = WM_USER + 2 };
};

#endif
